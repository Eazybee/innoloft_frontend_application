import React from 'react';
import { Provider } from 'react-redux';
import styled from 'styled-components';
import ErrorBoundary from '<components>/ui/ErrorBoundary/ErrorBoundary';
import GlobalStyle from '<styles>/Global';
import stores from '<redux>/stores';
import Routes from './Routes';

const Body = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
`;

const Content = styled.div`
  max-width: 90rem;
  width: 100%;
`;

const App = () => (
  <>
    <GlobalStyle />
    <ErrorBoundary>
      <Provider store={stores}>
        <Body>
          <Content>
            <Routes />
          </Content>
        </Body>
      </Provider>
    </ErrorBoundary>
  </>
);

export default App;
