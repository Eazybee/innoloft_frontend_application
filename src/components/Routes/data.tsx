import { FC } from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import Settings from '<components>/pages/Settings/Settings';
import { Routes } from '<helpers>/typings';

type RouteData = {
  default: {
    exact: boolean;
    path: string;
    protected?: boolean;
    Component: FC<RouteChildrenProps<any, unknown>> | ((prop?: unknown) => JSX.Element);
  }[];
};

const routes: RouteData = {
  default: [
    {
      exact: true,
      path: Routes.MySettings,
      Component: Settings,
    },
  ],
};

export default routes;
