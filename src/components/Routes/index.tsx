import React, { Suspense } from 'react';
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from 'react-router-dom';
import { css } from 'styled-components';
import LoadingSpinner from '../ui/LoadingSpinner/LoadingSpinner';
import routeData from './data';
import { Routes as RoutesName } from '<helpers>/typings';
import PageLayout from '<components>/PageLayout/PageLayout';

const Routes = () => (
  <Router>
    <PageLayout>
      <Suspense
        fallback={(
          <LoadingSpinner
            styles={css`
              height: 100vh;
              width: 100vw;
              display: flex;
              justify-content: center;
              align-items: center;
            `}
          />
        )}
      >
        <Switch>
          {routeData.default.map(({ exact, path, Component }) => (
            <Route key={path} exact={exact} path={path} component={Component} />
          ))}
          <Route
            path="*"
            render={({ location }) => (
              <Redirect
                to={{
                  pathname: RoutesName.MySettings,
                  state: { from: location },
                }}
              />
            )}
          />
        </Switch>
      </Suspense>
    </PageLayout>
  </Router>
);

export default Routes;
