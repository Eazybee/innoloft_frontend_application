import React, { FC, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled, { StyledComponent } from 'styled-components';
import useForm from '<hooks>/useForm';
import HorizontalTabs from '<components>/ui/HorizontalTabs/HorizontalTabs';
import { countries } from '<helpers>/constants';
import {
  Status, Selected, User, UserInfo,
} from '<helpers>/typings';
import UserAuthInfo from './Sections/UserAuthInfo/UserAuthInfo';
import UserAdditionalInfo from './Sections/UserAdditionalInfo/UserAdditionalInfo';
import { mockUser, updateUserInfo } from '<redux>/actions/users';
import { delay } from '<helpers>/utils';
import style from './style.css';

const tabs = ['Account Settings', 'User Information'];

const rules = {
  firstName: 'required|string',
  lastName: 'required|string',
  'house_/_block_/_flat_number': 'required|string',
  streetAddress: 'required|string',
  postalCode: 'required|numeric',
};

type Props = {
  mockUser: () => void;
  updateUserInfo: (body: UserInfo) => Promise<boolean>;
  user: User;
};

const SettingsPage: FC<Props> & {
  Style: StyledComponent<'div', any, {}>;
} = (props: Props) => {
  const [current, setCurrent] = useState(0);
  const [auth, setAuth] = useState({
    email: {
      value: '',
      error: '',
    },
    password: {
      value: '',
      error: '',
      strength: 0,
    },
    confirmPassword: {
      value: '',
      error: '',
      strength: 0,
    },
  });

  const [country, setCountry] = useState<Selected>(countries[0]);
  const [statuses, setStatuses] = useState({
    authentication: Status.IDLE,
    additional: Status.IDLE,
  });

  const { mockUser: populateMockUser, updateUserInfo: updateUserInformation, user } = props;
  useEffect(() => {
    if (populateMockUser) populateMockUser();
  }, [populateMockUser]);

  const updateAddition = async (vals: Inputs) => {
    const {
      firstName, lastName, postalCode, streetAddress,
    } = vals;

    const body: UserInfo = {
      firstName,
      lastName,
      postalCode,
      streetAddress,
      houseNo: vals['house_/_block_/_flat_number'],
      country: country.value,
    };

    setStatuses({
      ...statuses,
      additional: Status.LOADING,
    });

    const res = await updateUserInformation(body);

    if (res) {
      setStatuses({
        ...statuses,
        additional: Status.SUCCESS,
      });
    } else {
      setStatuses({
        ...statuses,
        additional: Status.FAIL,
      });
    }

    await delay(5000);

    return setStatuses({
      ...statuses,
      additional: Status.IDLE,
    });
  };

  const {
    values, handleChange, handleSubmit, errors, setValues,
  } = useForm({
    callback: updateAddition,
    rules,
  });

  useEffect(() => {
    if (user?.email) {
      setAuth((state) => ({
        ...state,
        email: {
          ...state.email,
          value: user.email,
        },
      }));

      setValues({
        firstName: user.firstName,
        lastName: user.lastName,
        'house_/_block_/_flat_number': user.houseNo,
        streetAddress: user.streetAddress,
        postalCode: user.postalCode,
      });

      if (user.country) {
        setCountry(countries.find((opt) => opt.value === user.country) || countries[0]);
      }
    }
  }, [setValues, user]);

  return (
    <SettingsPage.Style>
      <HorizontalTabs tabs={tabs} current={current} setCurrent={setCurrent} />
      <div>
        {current === 0 && user.email ? (
          <UserAuthInfo
            auth={auth}
            setAuth={setAuth}
            setStatuses={setStatuses}
            statuses={statuses}
          />
        ) : null}
        {current === 1 && user.email ? (
          <UserAdditionalInfo
            values={values}
            errors={errors}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
            country={country}
            setCountry={setCountry}
            statuses={statuses}
          />
        ) : null}
      </div>
    </SettingsPage.Style>
  );
};

SettingsPage.Style = styled.div`
  ${style}
`;

type Inputs = typeof rules;

const mapStateToProps = (state: { user: User }) => ({
  user: state.user,
});

export default connect(mapStateToProps, { mockUser, updateUserInfo })(SettingsPage);
