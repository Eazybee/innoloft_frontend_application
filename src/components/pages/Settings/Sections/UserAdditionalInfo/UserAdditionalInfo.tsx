import React, { FC } from 'react';
import { Status, Selected, ChangeEvent } from '<helpers>/typings';
import UserForm from '<components>/resuableSection/UserForm/UserForm';

const UserAdditionalInfo: FC<Props> = ({
  statuses,
  setCountry,
  values,
  errors,
  handleChange,
  handleSubmit,
  country,
}: Props) => {
  const inputs = [
    {
      value: values.firstName,
      error: errors.firstName,
      required: true,
      type: 'text',
      onChange: handleChange,
      label: 'First Name',
      name: 'firstName',
    },
    {
      value: values.lastName,
      error: errors.lastName,
      required: true,
      type: 'text',
      onChange: handleChange,
      label: 'Last Name',
      name: 'lastName',
    },
    {
      value: values['house_/_block_/_flat_number'],
      error: errors['house_/_block_/_flat_number'],
      required: true,
      type: 'text',
      onChange: handleChange,
      label: 'House/Block/Flat No',
      name: 'house_/_block_/_flat_number',
    },
    {
      value: values.streetAddress,
      error: errors.streetAddress,
      required: true,
      type: 'text',
      onChange: handleChange,
      label: 'Street Address',
      name: 'streetAddress',
    },
    {
      value: values.postalCode,
      error: errors.postalCode,
      required: true,
      type: 'text',
      onChange: handleChange,
      label: 'Postal Code',
      name: 'postalCode',
    },
  ];

  const disabled = statuses.additional === Status.LOADING;

  return (
    <UserForm
      handleSubmit={handleSubmit}
      success={statuses.additional === Status.SUCCESS ? 'Information updated successfully' : ''}
      error={
        statuses.additional === Status.FAIL
          ? 'Error: There was an error while updating your information'
          : ''
      }
      inputs={inputs}
      btnLabel={statuses.additional === Status.LOADING ? 'Loading...' : 'Update'}
      showCountry
      defaultCountry={country}
      setCountry={setCountry}
      disabled={disabled}
    />
  );
};

type Statuses = {
  authentication: Status;
  additional: Status;
};

type Props = {
  statuses: Statuses;
  country: Selected;
  setCountry: (e: Selected) => void;
  values: Record<string, any>;
  errors: Record<string, any>;
  handleSubmit: (e: Record<string, any>) => void;
  handleChange: (e: ChangeEvent) => void;
};

export default UserAdditionalInfo;
