import React, { FC } from 'react';
import { connect } from 'react-redux';
import { emailRegEx } from '<helpers>/constants';
import {
  Status, ChangeEvent, UserAuthInfomation, Auth,
} from '<helpers>/typings';
import UserForm from '<components>/resuableSection/UserForm/UserForm';
import { updateUserAuth } from '<redux>/actions/users';
import { delay, passwordStrengthText, validatePasswordChange } from '<helpers>/utils';

const UserAuthInfo: FC<Props> = ({
  auth,
  setAuth,
  statuses,
  setStatuses,
  updateUserAuthentication,
}: Props) => {
  const handlePasswordChange = ({ target }: ChangeEvent) => {
    const { value, name } = target;
    const { strength, error } = validatePasswordChange(value);

    setAuth({
      ...auth,
      [name]: { value, strength, error },
    });
  };

  const handlePasswordBlur = () => {
    let error = '';

    if (auth.password.value !== auth.confirmPassword.value) {
      error = 'Password do not match';
    }

    if (error) {
      setAuth({
        ...auth,
        confirmPassword: {
          ...auth.confirmPassword,
          error,
        },
      });
    }
  };

  const handleEmail = ({ target }: ChangeEvent) => {
    const { name, value } = target;
    let error = '';

    if (value.trim() === '') {
      error = 'Field is required';
    } else if (!emailRegEx.test(value.trim())) {
      error = 'Invalid email format';
    }

    setAuth({
      ...auth,
      [name]: { value, error },
    });
  };

  const updateAuth = async (e: React.FormEvent) => {
    e.preventDefault();

    const emailError = (() => {
      let error = '';

      if (auth.email.value.trim() === '') {
        error = 'Field is required';
      } else if (!emailRegEx.test(auth.email.value.trim())) {
        error = 'Invalid email format';
      }

      return error;
    })();

    const password = validatePasswordChange(auth.password.value);
    const rePassword = validatePasswordChange(auth.confirmPassword.value);

    if (emailError || password.error || rePassword.error) {
      return setAuth({
        ...auth,
        email: {
          ...auth.email,
          error: emailError,
        },
        password: {
          ...auth.password,
          ...password,
        },
        confirmPassword: {
          ...auth.confirmPassword,
          ...rePassword,
        },
      });
    }

    if (auth.password.value !== auth.confirmPassword.value) {
      return setAuth({
        ...auth,
        confirmPassword: {
          ...auth.confirmPassword,
          error: 'Password do not match',
        },
      });
    }

    const body = {
      email: auth.email.value,
      password: auth.password.value,
    };

    setStatuses({
      ...statuses,
      authentication: Status.LOADING,
    });
    const res = await updateUserAuthentication(body);

    if (res) {
      setStatuses({
        ...statuses,
        authentication: Status.SUCCESS,
      });

      setAuth({
        ...auth,
        password: {
          value: '',
          error: '',
          strength: 0,
        },
        confirmPassword: {
          value: '',
          error: '',
          strength: 0,
        },
      });
    } else {
      setStatuses({
        ...statuses,
        authentication: Status.FAIL,
      });
    }

    await delay(5000);
    return setStatuses({
      ...statuses,
      authentication: Status.IDLE,
    });
  };

  const inputs = [
    {
      value: auth.email.value,
      error: auth.email.error,
      type: 'text',
      onChange: handleEmail,
      label: 'Email',
      name: 'email',
      required: true,
    },
    {
      value: auth.password.value,
      type: 'password',
      onChange: handlePasswordChange,
      label: 'New Password',
      name: 'password',
      passWordPercentage: auth.password.strength,
      error: auth.password.error,
      passwordStrength: passwordStrengthText(auth.password.strength),
      required: true,
    },
    {
      value: auth.confirmPassword.value,
      type: 'password',
      onChange: handlePasswordChange,
      onBlur: handlePasswordBlur,
      label: 'Confirm Password',
      name: 'confirmPassword',
      error: auth.confirmPassword.error,
      passWordPercentage: auth.confirmPassword.strength,
      passwordStrength: passwordStrengthText(auth.confirmPassword.strength),
      required: true,
    },
  ];

  const disabled = statuses.authentication === Status.LOADING;

  return (
    <UserForm
      handleSubmit={updateAuth}
      success={statuses.authentication === Status.SUCCESS ? 'Information updated successfully' : ''}
      error={
        statuses.authentication === Status.FAIL
          ? 'Error: There was an error while updating your information'
          : ''
      }
      inputs={inputs}
      btnLabel={statuses.authentication === Status.LOADING ? 'Loading...' : 'Update'}
      disabled={disabled}
    />
  );
};

type Statuses = {
  authentication: Status;
  additional: Status;
};

type Props = {
  auth: UserAuthInfomation;
  setAuth: (e: UserAuthInfomation) => void;
  statuses: Statuses;
  setStatuses: (e: Statuses) => void;
  updateUserAuthentication: (body: Auth) => Promise<boolean>;
};

export default connect(null, { updateUserAuthentication: updateUserAuth })(UserAuthInfo);
