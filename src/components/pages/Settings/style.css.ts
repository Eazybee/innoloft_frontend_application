import { css } from 'styled-components';

const styled = css`
  width: 40rem;
  background-color: white;

  > div {
    display: flex;
    justify-content: center;
    align-items: center;
    padding-bottom: 1rem;
  }

  @media screen and (max-width: 840px) {
    width: 100%;
  }
`;

export default styled;
