import { css } from 'styled-components';

const styled = css`
  label.mdropDownLabel {
    font-size: 0.8rem;
    color: #565a61;
    position: relative;
    top: 0.5rem;
    z-index: 1;
    margin-left: 0.7rem;
    background: white;
    width: fit-content;
    padding: 0 0.3rem;
  }

  .mdropDown {
    background-color: #ffffff;
    position: relative !important;
    height: 100%;
  }

  .mdropDown button {
    display: block;
    background-color: transparent;
    font-size: 0.8em;
    width: 100%;
    border: none;
    text-align: left;
    overflow: hidden;
    text-overflow: ellipsis;
    padding: 0.8rem 0.7rem 0.8rem 0.7rem;
    line-height: 1.5rem;
  }

  .mdropDown > button {
    border: 1px solid #a9adb3;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 0.7rem;
  }

  .mdropDown > button p {
    flex: 1;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size: 1rem;
  }

  .mdropDown > button svg {
    transition: 0.5s;
    transform: rotate(90deg);
    font-size: 9px;
    margin-right: 0.1rem;
  }

  .mdropDown div {
    background-color: #ffffff;
    position: absolute !important;
    display: block !important;
    z-index: 100;
    min-width: 100% !important;
    width: auto !important;
    box-shadow: 1px -1px 16px 10px rgb(169 169 169 / 20%);
    right: 0;
    top: 0.1rem;
  }

  .mdropDown div button {
    background-color: #ffffff;
    display: flex;
    align-items: center;
  }

  .mdropDown div button:hover {
    background-color: #e6f2ff;
  }
  .mdropDown div button.selected {
    background-color: #b2d3f7;
  }
  .mdropDown div button.selected:hover {
    background-color: #90c1f7;
  }

  .mdropDown div span,
  .mdropDown div svg {
    font-size: 0.7rem;
    display: block;
    margin-right: 5px;
  }

  @media screen and (max-width: 678px) {
    .mdropDown {
      width: auto;
    }
  }
`;

export default styled;
