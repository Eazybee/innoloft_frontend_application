/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/require-default-props */
import React, { useState, useEffect, FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGreaterThan, faCheck } from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';
import useComponentVisible from '<hooks>/useComponentVisible';
import { Selected } from '<helpers>/typings';
import style from './style.css';

const Select: FC<SelectProps> & any = ({
  onClose,
  options,
  label,
  disabled,
  defaultValue,
}: SelectProps) => {
  const defaultOption = defaultValue || options[0];
  const [selection, setSelection] = useState<Selected>(defaultOption);
  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  const makeSelection = (selected: Selected) => {
    setSelection(selected);
    setIsComponentVisible(false);
  };

  useEffect(() => {
    if (!isComponentVisible && selection) onClose(selection);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isComponentVisible, selection]);

  const selectionText = (() => {
    const selected = options.find((option) => option.value === selection.value);
    return selected ? selected.label : '';
  })();

  const toggleHandler = () => {
    if (isComponentVisible) {
      onClose(selection);
    }
    setIsComponentVisible(!isComponentVisible);
  };

  return (
    <Select.Style>
      <label className="mdropDownLabel">{label}</label>
      <div className="mdropDown" ref={ref}>
        <button
          role="combobox"
          aria-controls="rtTYDU"
          aria-expanded={isComponentVisible}
          onClick={toggleHandler}
          title={selectionText}
          disabled={disabled}
          type="button"
        >
          <p>{selectionText}</p>
          <FontAwesomeIcon icon={faGreaterThan} />
        </button>
        {isComponentVisible && (
          <div id="rtTYDU">
            {options.map(({ value, label: lbl }) => (
              <button
                role="option"
                aria-selected={value === selection.value}
                onClick={() => makeSelection({ value, label: lbl })}
                value={value}
                className={value === selection.value ? 'selected' : ''}
                key={value}
                type="button"
              >
                {value === selection.value ? <FontAwesomeIcon icon={faCheck} /> : <span />}
                {lbl}
              </button>
            ))}
          </div>
        )}
      </div>
    </Select.Style>
  );
};

Select.Style = styled.div`
  ${style}
`;

type SelectProps = {
  label: string;
  options: Selected[];
  disabled: boolean;
  defaultValue: Selected;
  onClose: (selections: Selected) => any;
};

export default Select;
