import { css } from 'styled-components';

const styled = css`
  width: 100%;
  display: flex;
  flex-flow: column-reverse;

  label {
    margin-bottom: 0.3rem;
    position: relative;
    font-size: 0.8rem;
    color: #565a61;
    transition: 0.5s;
    width: fit-content;
    top: 0.7rem;
    background: white;
    margin-left: 0.7rem;
    padding: 0 0.3rem;
  }
  textarea,
  input {
    border: 1px solid #d5dae2;
    font-size: 1rem;
    border-radius: 0.7rem;
    padding: 0.8rem 0.7rem 0.8rem 0.7rem;
    line-height: 1.5rem;

    &::placeholder,
    &::-webkit-input-placeholder {
      font-size: 0.8rem;
      color: transparent;
    }

    &.empty + label {
      top: 2.5rem;
    }
    &:not(.empty) + label {
      top: 0.7rem;
    }
    &:focus {
      outline-color: #00a9dd;
      & + label {
        top: 0.7rem;
      }
    }
  }
  p.error {
    color: red;
    font-size: 0.8rem;
  }

  p.password {
    font-size: 0.7rem;
    display: flex;
    align-items: center;

    > span {
      display: flex;
      margin: 0 0.2rem;
      flex: 1;
      height: 0.13rem;
      background: #ff0000;

      span {
        display: flex;
        height: 100%;
        background: #008000;
        transition: 1s;
        width: 0%;
      }
    }
  }
`;

export default styled;
