import React, { RefObject } from 'react';
import styled from 'styled-components';
import styles from './style.css';

const Styled = styled.div`
  ${styles}
`;

const Input = React.forwardRef(
  (
    {
      label, error, name, value, type, passWordPercentage, passwordStrength, ...rest
    }: Props,
    ref:
    | string
    | ((instance: HTMLInputElement | null) => void)
    | RefObject<HTMLInputElement>
    | null
    | undefined,
  ) => (
    <Styled>
      {error && <p className="error">{error}</p>}
      {type === 'password' && (
        <p className="password">
          {passwordStrength}
          <span>
            <span style={{ width: `${passWordPercentage}%` }} />
          </span>
        </p>
      )}
      <input
        id={`${label}${name}`}
        value={value}
        className={`${value?.trim() ? '' : 'empty'}`}
        name={name}
        type={type}
        {...rest}
        ref={ref}
      />
      <label htmlFor={`${label}${name}`}>{label}</label>
    </Styled>
  ),
);

export interface Props
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  label: string;
  value: string;
  error?: string;
  onChange: (event: React.ChangeEvent<any>) => any;
  ref?: any;
  name?: string;
  passwordStrength?: string;
  passWordPercentage?: number;
}

Input.defaultProps = {
  type: 'text',
};

export default Input;
