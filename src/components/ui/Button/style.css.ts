import { css } from 'styled-components';

const styled = css`
  background: #157646;
  color: white;
  padding: 0 2rem;
  height: 3rem;
  border-radius: 0.5rem;
  box-shadow: -1px 9px 10px rgba(0, 0, 0, 0.25);
  cursor: pointer;
  position: relative;
  transition: 1s;
  opacity: 0.8;

  &:hover,
  &:focus {
    bottom: 0.05rem;
    right: 0.05rem;
    opacity: 1;
  }
`;

export default styled;
