import React from 'react';
import styled from 'styled-components';
import style from './style.css';

type ButtonType = 'button' | 'reset' | 'submit';

interface Props
  extends React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
  > {
  children: any;
  type: ButtonType;
  styles?: string;
}

const Button = React.forwardRef(({ children, ...rest }: Props, ref: any) => (
  <Styled ref={ref} {...rest}>
    {children}
  </Styled>
));

const Styled = styled.button`
  ${style}
  ${({ styles }: Props) => `
    ${styles}
  `}
`;

export default Button;
