import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';
import style from './style.css';

const HorizontalTabs: FC<Props> & {
  Style: StyledComponent<'nav', any, {}>;
} = ({ tabs, current, setCurrent }: Props) => (
  <HorizontalTabs.Style>
    <ul>
      {tabs.map((tab, ind: number) => (
        <li key={tab}>
          <button
            type="button"
            className={`${current === ind ? 'active' : ''}`}
            onClick={() => setCurrent(ind)}
          >
            {tab}
          </button>
        </li>
      ))}
    </ul>
  </HorizontalTabs.Style>
);

HorizontalTabs.Style = styled.nav`
  ${style}
`;

type Props = {
  tabs: string[];
  current: number;
  setCurrent: (cur: number) => void;
};

export default HorizontalTabs;
