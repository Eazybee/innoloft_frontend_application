import { css } from 'styled-components';

const styled = css`
  background-color: white;

  ul {
    display: flex;

    li {
      flex: 1;

      button {
        width: 100%;
        height: 100%;
        padding: 0.7rem;
        background-color: white;

        &.active {
          background-color: #d8d8d8;
        }
      }
    }
  }
`;

export default styled;
