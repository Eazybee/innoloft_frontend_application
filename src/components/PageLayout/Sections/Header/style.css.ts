import { css } from 'styled-components';

const style = css`
  display: flex;
  justify-content: space-between;
  padding: 1rem 1.5rem;
  background: #dbe06d;
  background: linear-gradient(90deg, #dbe06d 8%, #0e7764 75%);
  box-shadow: 0px 1px 8px 0px #00000078;

  & > button {
    width: 2rem;
    border-radius: 0.3rem;
  }

  & > div.logo {
    img {
      height: 1.5rem;
    }
  }

  & > div:last-child {
    width: 10rem;
    display: flex;
    justify-content: space-between;
    align-items: center;

    svg path {
      fill: white;
    }

    & > span {
      display: flex;
      align-items: flex-end;

      &::after {
        display: block;
        background: #fdc013;
        content: "";
        width: 0.5rem;
        height: 0.5rem;
        position: relative;
        border-radius: 50%;
        border: 1px solid white;
        top: 0.2rem;
        right: 0.2rem;
      }
    }

    div {
      display: flex;
      align-items: center;

      p {
        margin-left: 0.5rem;
        color: white;
        font-size: 0.9rem;
      }
    }
  }

  @media screen and (max-width: 800px) {
    background: linear-gradient(90deg, #dbe06d 20%, #0e7764 75%);
    padding: 1rem;
  }

  @media screen and (max-width: 506px) {
    background: linear-gradient(90deg, #dbe06d 30%, #0e7764 75%);

    & > div.logo {
      h1 {
        font-size: 1.5rem;

        span.loft {
          font-size: 0.7rem;
          height: 1.08rem;
          position: relative;
          top: 1px;
        }
      }
    }

    & > div:last-child {
      width: 5rem;

      div p {
        display: none;
      }
    }
  }

  @media screen and (max-width: 400px) {
    padding: 1rem 0.3rem;
  }
`;

export default style;
