import React, { FC, useCallback, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGlobeAsia,
  faEnvelope,
  faBell,
  faBars,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import styled, { StyledComponent } from 'styled-components';
import Innoloft from '<assests>/innoloft-no-white-space.svg';
import style from './style.css';
import useWindowResizeWidth from '<hooks>/useWindowResizeWidth';
import { MenuProps } from '<helpers>/typings';

const Header: FC<MenuProps> & {
  Style: StyledComponent<'header', any, {}>;
} = ({ menu, setMenu }: MenuProps) => {
  const windowWidth = useWindowResizeWidth();
  const { toggle } = menu;

  useEffect(() => {
    if (windowWidth <= 840 && menu.hidden) {
      setMenu({
        toggle: false,
        hidden: false,
      });
    } else if (windowWidth > 840 && !menu.hidden) {
      setMenu({
        toggle: false,
        hidden: true,
      });
    }
  }, [menu.hidden, setMenu, windowWidth]);

  const onCLick = useCallback(() => {
    setMenu((stl) => ({
      ...stl,
      toggle: !toggle,
    }));
  }, [setMenu, toggle]);

  return (
    <Header.Style>
      {!menu.hidden ? (
        <button
          arial-label={toggle ? 'close menu' : 'show menu'}
          title={toggle ? 'close menu' : 'show menu'}
          onClick={onCLick}
          type="button"
        >
          <FontAwesomeIcon icon={toggle ? faTimes : faBars} />
        </button>
      ) : null}
      <div className="logo">
        <img src={Innoloft} alt="Innoloft logo" />
      </div>
      <div>
        <div>
          <FontAwesomeIcon icon={faGlobeAsia} />
          <p>EN</p>
        </div>
        <span>
          <FontAwesomeIcon icon={faEnvelope} />
        </span>
        <span>
          <FontAwesomeIcon icon={faBell} />
        </span>
      </div>
    </Header.Style>
  );
};

Header.Style = styled.header`
  ${style}
`;

export default Header;
