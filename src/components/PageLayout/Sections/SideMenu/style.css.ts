import { css } from 'styled-components';

const style = css`
  max-width: 30rem;
  display: flex;
  justify-content: center;
  flex: 1;
  padding: 4rem 0;

  ul {
    padding-right: 2rem;

    li {
      svg {
        margin-right: 0.8rem;
      }

      &:not(:last-child) {
        margin-bottom: 2rem;
      }

      a,
      path {
        transition: 0.5s;
        color: black;
        fill: black;
        white-space: nowrap;
      }
      a.active,
      a:hover {
        color: #f39f20;

        path {
          fill: #f39f20;
        }
      }
    }
  }

  @media screen and (max-width: 840px) {
    flex: none;
    height: 100%;
    padding: 0;
    z-index: 2;
    position: absolute;
    transition: background 1s;
    ${({ toggle }: any) => (toggle
    ? `
      width: 100vw;
      justify-content: flex-start;
      max-width: 100%;
      min-width: 320px;
      background: #00000063;
      left: 0;
      `
    : '')}

    ul {
      height: 100%;
      left: ${({ toggle }: any) => (toggle ? '0rem' : '-16rem')};
      position: absolute;
      transition: left 1s, width 0s;
      background: #f3f3f3;
      padding: 0;
      width: 15rem;
      box-shadow: 2px 2px 3px #00000038;
      li {
        margin-bottom: 0 !important;
        display: flex;

        a {
          width: 100%;
          padding: 1rem 0.5rem;
          border-bottom: 0.1rem solid #d6d3d3;

          &:hover,
          &:focus,
          &.active {
            background: #dddddd;
          }
        }
      }
    }
  }
`;

export default style;
