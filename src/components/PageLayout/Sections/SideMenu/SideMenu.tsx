import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';
import styled, { StyledComponent } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHome,
  faBullhorn,
  faChartArea,
  faBuilding,
  faCog,
} from '@fortawesome/free-solid-svg-icons';
import { faNewspaper } from '@fortawesome/free-regular-svg-icons';
import { MenuProps, Routes } from '<helpers>/typings';
import style from './style.css';

const SideMenu: FC<MenuProps> & {
  Style: StyledComponent<'nav', any, { toggle: boolean }>;
} = ({ menu }: MenuProps) => {
  const { toggle } = menu;

  return (
    <SideMenu.Style toggle={toggle}>
      <ul>
        <li>
          <NavLink exact to={Routes.Home}>
            <FontAwesomeIcon icon={faHome} />
            Home
          </NavLink>
        </li>
        <li>
          <NavLink exact to={Routes.MyAccount}>
            <FontAwesomeIcon icon={faBullhorn} />
            My Account
          </NavLink>
        </li>
        <li>
          <NavLink exact to={Routes.MyCompany}>
            <FontAwesomeIcon icon={faBuilding} />
            My Company
          </NavLink>
        </li>
        <li>
          <NavLink exact to={Routes.MySettings}>
            <FontAwesomeIcon icon={faCog} />
            My Settings
          </NavLink>
        </li>
        <li>
          <NavLink exact to={Routes.News}>
            <FontAwesomeIcon icon={faNewspaper} />
            News
          </NavLink>
        </li>
        <li>
          <NavLink exact to={Routes.Analytics}>
            <FontAwesomeIcon icon={faChartArea} />
            Analytics
          </NavLink>
        </li>
      </ul>
    </SideMenu.Style>
  );
};

SideMenu.Style = styled.nav`
  ${style}
`;

export default SideMenu;
