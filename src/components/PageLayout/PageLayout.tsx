import React, { FC, useState } from 'react';
import styled from 'styled-components';
import Header from './Sections/Header/Header';
import SideMenu from '<components>/PageLayout/Sections/SideMenu/SideMenu';

const PageLayout: FC<{}> = ({ children }: any) => {
  const [menu, setMenu] = useState({
    toggle: false,
    hidden: true,
  });

  return (
    <Main>
      <Header menu={menu} setMenu={setMenu} />
      <Content>
        <SideMenu menu={menu} setMenu={setMenu} />
        <div>{children}</div>
      </Content>
    </Main>
  );
};

const Main = styled.main``;

const Content = styled.div`
  padding: 0 1rem;
  display: flex;

  & > div {
    padding: 4rem 0;
    display: flex;
    flex: 1;
  }

  @media screen and (max-width: 400px) {
    padding: 0 0.5rem;
  }
`;

export default PageLayout;
