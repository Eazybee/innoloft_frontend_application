import { css } from 'styled-components';

const styled = css`
  & > p {
    font-size: 0.8rem;

    &.error,
    &.success {
      position: relative;
      top: 1rem;
      padding-bottom: 0.2rem;
    }

    &.error {
      color: red;
    }

    &.success {
      color: #117111;
    }
  }

  & > div:last-child {
    margin-top: 1rem;
  }
`;

export default styled;
