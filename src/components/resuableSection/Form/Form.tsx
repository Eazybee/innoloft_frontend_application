import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';
import style from './style.css';

const Form: FC<Props> & {
  Styled: StyledComponent<'form', any, {}>;
} = ({
  handleSubmit, error, success, children,
}: Props) => (
  <Form.Styled onSubmit={handleSubmit}>
    {error ? <p className="error">{error}</p> : ''}
    {success ? <p className="success">{success}</p> : ''}
    {children}
  </Form.Styled>
);

Form.Styled = styled.form`
  ${style}
`;

type Props = {
  handleSubmit: (e: any) => void;
  error?: string;
  success?: string;
  children?: JSX.Element;
};

export default Form;
