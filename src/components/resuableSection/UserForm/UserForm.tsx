import React, { FC } from 'react';
import styled, { StyledComponent } from 'styled-components';
import Input, { Props as InputProps } from '<components>/ui/Input/Input';
import Form from '<components>/resuableSection/Form/Form';
import Button from '<components>/ui/Button/Button';
import Select from '<components>/ui/Select';
import { countries } from '<helpers>/constants';
import { Selected } from '<helpers>/typings';
import style from './style.css';

const UserForm: FC<Props> & {
  Style: StyledComponent<'div', any, {}>;
} = ({
  handleSubmit,
  inputs,
  showCountry,
  setCountry,
  btnLabel,
  success,
  error,
  disabled,
  defaultCountry,
}: Props) => (
  <UserForm.Style>
    <Form handleSubmit={handleSubmit} success={success} error={error}>
      <>
        <div>
          {inputs.map(({ label, ...inputProps }: any) => (
            <Input key={label} label={label} disabled={disabled} {...inputProps} />
          ))}
        </div>
        {showCountry ? (
          <Select
            label="Country"
            options={countries}
            defaultValue={defaultCountry}
            onClose={setCountry}
            disabled={disabled}
          />
        ) : null}
        <div className="submit">
          <Button type="submit" disabled={disabled}>
            {btnLabel}
          </Button>
        </div>
      </>
    </Form>
  </UserForm.Style>
);

UserForm.Style = styled.div`
  ${style}
`;

type Props = {
  handleSubmit: (e: any) => void;
  inputs: InputProps[];
  btnLabel: string;
  disabled: boolean;
  showCountry?: boolean;
  defaultCountry?: Selected;
  setCountry?: (e: Selected) => void;
  success?: string;
  error?: string;
};

export default UserForm;
