import { css } from 'styled-components';

const styled = css`
  width: 60%;
  .submit {
    display: flex;
    justify-content: center;
    margin-top: 1rem;
  }

  @media screen and (max-width: 506px) {
    width: 80%;
  }

  @media screen and (max-width: 400px) {
    width: 90%;
    .submit button {
      width: 100%;
    }
  }
`;

export default styled;
