import { useState } from 'react';
import Validator from 'validatorjs';
import formatter from '../helpers/formatter';

const useForm = ({ callback, rules }: { callback: any; rules: Record<string, any> }) => {
  const initialState = () => {
    const state: Record<string, any> = {};
    Object.keys(rules).forEach((key) => {
      state[key] = '';
    });
    return state;
  };

  const [values, setValues] = useState(initialState());
  const [errors, setErrors] = useState({});

  Validator.setAttributeFormatter((attribute: string) => formatter(attribute));

  const validateOnSubmit = () => {
    let hasError = true;
    const newErrors: Record<string, any> = { ...errors };

    Object.keys(rules).forEach((key) => {
      const validate = (name: string, value: string) => {
        const validation = new Validator({ [name]: value }, { [name]: rules[name] });

        const errorMessage = validation.fails() && validation.errors.first(name);

        if (errorMessage) {
          newErrors[name] = errorMessage;
          hasError = false;
        } else {
          delete newErrors[name];
        }
      };

      if (Array.isArray(values[key])) {
        values[key].forEach((value: string) => validate(key, value));
      } else {
        validate(key, values[key]);
      }
    });

    setErrors({ ...newErrors });
    return hasError;
  };

  const errorHandler = (name: string, value: string, message: string) => {
    setErrors({
      ...errors,
      [name]: message,
    });
    setValues({
      ...values,
      [name]: value,
    });

    return false;
  };

  const validateOnChange = (event: any) => {
    const { target } = event;
    const { required, name, type } = target;
    let { value } = target;

    if (type === 'checkbox') {
      value = target.checked;
      if (required && !value) {
        return errorHandler(name, value, `The ${formatter(name)} must be accepted.`);
      }
    } else if (required && value.trim() === '') {
      return errorHandler(name, value, `The ${formatter(name)} field cannot be empty.`);
    }

    const validation = new Validator({ [name]: value }, { [name]: rules[name] });

    if (validation.fails()) {
      // @ts-ignore
      return errorHandler(name, value, validation.errors.first(name));
    }
    return true;
  };

  const getMultipleSelection = (event: any) => {
    const { options } = event.target;
    const selected = Object.entries(options).filter(([, option]: any) => option.selected);
    const value = selected.map(([, option]: any) => option.value);
    return value;
  };

  const handleChange = (event: any) => {
    const { target } = event;
    if (validateOnChange(event)) {
      let { value } = target;

      if (target.type === 'select-multiple') {
        value = getMultipleSelection(event);
      }
      if (target.type === 'checkbox') {
        value = target.checked;
      }

      setValues({
        ...values,
        [target.name]: value,
      });

      // @ts-ignore
      delete errors[target.name];
      setErrors({ ...errors });
      return true;
    }
    return false;
  };

  const sanitizeData = () => {
    const data: Record<string, any> = {};
    Object.keys(values).forEach((field) => {
      if (Array.isArray(values[field])) {
        data[field] = values[field].map((value: string) => value.trim());
      } else if (typeof values[field] === 'boolean') {
        data[field] = values[field];
      } else {
        data[field] = values[field].trim();
      }
    });
    return data;
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (validateOnSubmit()) {
      const data = sanitizeData();
      return callback(data);
    }

    return false;
  };

  const handleReset = () => {
    setValues(initialState());
    setErrors({});
  };

  return {
    values,
    handleChange,
    handleSubmit,
    errors,
    handleReset,
    setValues,
  };
};

export default useForm;
