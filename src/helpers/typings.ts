export type Auth = { email: string; password: string };

export enum Routes {
  Home = '/',
  MyAccount = '/account',
  MyCompany = '/company',
  MySettings = '/settings',
  News = '/news',
  Analytics = '/analytics',
}

export type Selected = {
  value: string;
  label: string;
};

export type AxiosResponse = {
  statusCode: number;
  data: {
    status: 'success' | 'error';
  };
};

export type AxiosRequest = {
  patch: (url: string, body: Record<string, Record<string, any>>) => Promise<AxiosResponse>;
};

export enum Status {
  IDLE = 'IDLE',
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  FAIL = 'FAIL',
}

export type ChangeEvent = {
  target: {
    value: string;
    name: string;
  };
};

export type UserAuthInfomation = {
  email: {
    value: string;
    error: string;
  };
  password: {
    value: string;
    error: string;
    strength: number;
  };
  confirmPassword: {
    value: string;
    error: string;
    strength: number;
  };
};

type Menu = {
  toggle: boolean;
  hidden: boolean;
};

export type MenuProps = {
  menu: Menu;
  setMenu: React.Dispatch<React.SetStateAction<Menu>>;
};

type Email = {
  email: string;
};

export type UserInfo = {
  firstName: string;
  lastName: string;
  houseNo: string;
  streetAddress: string;
  postalCode: string;
  country: string;
};

export type User = Email & UserInfo;
