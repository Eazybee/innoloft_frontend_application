import { AxiosRequest } from './typings';

export const fakeAxios: AxiosRequest = {
  patch: (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    url: string, body: Record<string, Record<string, any>>,
  ) => new Promise((res) => setTimeout(
    () => res({
      statusCode: 200,
      data: {
        status: 'success',
      },
    }),
    2000,
  )),
};

export const delay = (time: number) => new Promise((res) => setTimeout(() => res(), time));

export const validatePasswordChange = (value: string) => {
  let strength = 0;
  let error = '';

  if (value === '') {
    error = 'Field is required';
  } else if (/[ ]/.test(value)) {
    error = 'Alphabetical, numbers and special characters only allowed';
  } else if (value.length < 8) {
    error = 'Minimum password length is 8';
  } else {
    strength += 20;

    // contain uppercaser
    if (/[A-Z]/.test(value)) {
      strength += 20;
    }

    // contain lowercase
    if (/[a-z]/.test(value)) {
      strength += 20;
    }

    // contain number
    if (/[0-9]/.test(value)) {
      strength += 20;
    }

    // contain special character
    if (/[!@#$%^&*(),.?":{}|<>]/.test(value)) {
      strength += 20;
    }
  }

  return { strength, error };
};
export const passwordStrengthText = (score: number) => {
  let text = '';
  switch (score) {
    case 0:
    case 20:
    case 40:
      text = 'Weak';
      break;
    case 60:
      text = 'Good';
      break;
    case 80:
      text = 'Very Good';
      break;
    default:
      text = 'Execellent';
  }

  return text;
};
