export const isDevelopment = process.env.MODE === 'development';

export const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const countries = [
  { label: 'Germany', value: 'germany' },
  { label: 'Austria', value: 'austria' },
  { label: 'Switzerland', value: 'switzerland' },
];
