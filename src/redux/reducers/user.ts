import { UserType } from '../actions/types';

type Action = {
  type: UserType;
  payload: {
    email: string;
  } & {
    firstName: '';
    lastName: '';
    houseNo: '';
    streetAddress: '';
    postalCode: '';
    country: '';
  };
};

export default (state = {}, action: Action) => {
  const { payload } = action;
  switch (action.type) {
    case UserType.MOCK_USER:
      return {
        ...state,
        ...payload,
      };
    case UserType.UPDATE_USER_AUTH:
      return {
        ...state,
        email: payload.email,
      };
    case UserType.UPDATE_USER_INFO:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
};
