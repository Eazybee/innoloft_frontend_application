import { Auth, AxiosResponse, UserInfo } from '<helpers>/typings';
import { delay, fakeAxios } from '<helpers>/utils';
import { UserType } from './types';

export const updateUserAuth = (body: Auth) => async (dispatch: any) => {
  try {
    const res: AxiosResponse = await fakeAxios.patch('https://fakeurl.com/api/auth', { body });
    await delay(5000);

    if (res.statusCode === 200 && res.data.status === 'success') {
      dispatch({
        type: UserType.UPDATE_USER_AUTH,
        payload: {
          email: body.email,
        },
      });
      return true;
    }
    throw new Error();
  } catch (error) {
    return false;
  }
};

export const updateUserInfo = (body: UserInfo) => async (dispatch: any) => {
  try {
    const res: AxiosResponse = await fakeAxios.patch('https://another-fakeurl.com/api/auth', {
      body,
    });
    await delay(5000);

    if (res.statusCode === 200 && res.data.status === 'success') {
      dispatch({
        type: UserType.UPDATE_USER_INFO,
        payload: body,
      });
      return true;
    }

    throw new Error();
  } catch (error) {
    return false;
  }
};

export const mockUser = () => (dispatch: any) => {
  dispatch({
    type: UserType.MOCK_USER,
    payload: {
      email: 'ilorieazykiel@gmail.com',
      firstName: 'Ilori',
      lastName: 'Ezekiel',
      houseNo: '',
      streetAddress: '',
      postalCode: '',
      country: 'switzerland',
    },
  });
};
