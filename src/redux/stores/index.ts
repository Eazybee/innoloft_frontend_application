/* eslint-disable no-underscore-dangle */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { isDevelopment } from '<helpers>/constants';
import reducers from '../reducers';

const middleware = [thunk];

const composeParam = [applyMiddleware(...middleware)];
// @ts-ignore
if (isDevelopment && window.__REDUX_DEVTOOLS_EXTENSION__) {
  // @ts-ignore
  composeParam.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}

const store = createStore(reducers, compose(...composeParam));

export default store;
